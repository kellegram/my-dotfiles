# Get a confirmation before an overwrite
alias mv='mv -i'
alias cp="cp -i"
alias rm='rm -i'


# Get color in grep output
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

## Get color in ls output
alias ls='lsd'

# Get more readable space output
alias df='df -h'
alias free='free -m'

# Get error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# Get covid stats
alias covuk='curl "https://corona-stats.online/UK?source=2"'
alias covstats='curl "https://corona-stats.online?top=30&source=2"'
alias covglobal='curl https://corona-stats.online'

# Update aliases
alias fu='flatpak update'
alias uu='sudo apt update -y && sudo apt upgrade -y && sudo apt upgrade -y'


## Misc aliases
alias untar='tar -zxvf'
alias wget='wget -c'
alias extip='curl ipinfo.io/ip && echo'


alias 11ty='npx @11ty/eleventy'
