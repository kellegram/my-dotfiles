#!/bin/sh
sed -i \
         -e 's/rgb(0%,0%,0%)/#1a1a18/g' \
         -e 's/rgb(100%,100%,100%)/#f4f4f4/g' \
    -e 's/rgb(50%,0%,0%)/#1a1a18/g' \
     -e 's/rgb(0%,50%,0%)/#d6c9de/g' \
 -e 's/rgb(0%,50.196078%,0%)/#d6c9de/g' \
     -e 's/rgb(50%,0%,50%)/#1a1a18/g' \
 -e 's/rgb(50.196078%,0%,50.196078%)/#1a1a18/g' \
     -e 's/rgb(0%,0%,50%)/#f4f4f4/g' \
	"$@"
